﻿using System.Collections.Generic;
using DialogEdit.dialog;
using Modding;

namespace radiantpapers
{
    // ReSharper disable once InconsistentNaming
    public class RadiantPapers : Mod
    {
        private struct god_language_strings
        {
            // example: Devara's
            public string possessive;
            public string possessiveReplace;

            // example: Devara
            public string regular;
            public string regularReplace;

            public bool active;
        }

        private static readonly god_language_strings[] LANGUAGE_STRINGS = new[]
        {
            // English
            new god_language_strings()
            {
                possessive = "Devara's",
                possessiveReplace = "Papers'",

                regular = "Devara",
                regularReplace = "Papers",

                active = true
            },

            // French
            new god_language_strings()
            {
                possessive = "Devara",
                possessiveReplace = "Papers",

                regular = "Devara",
                regularReplace = "Papers",

                active = true
            },

            // Italian
            new god_language_strings()
            {
                possessive = "Devara",
                possessiveReplace = "Papers",

                regular = "Devara",
                regularReplace = "Papers",

                active = true
            },

            // German?
            new god_language_strings()
            {
                // Google translate says this is right.
                possessive = "Devaras",
                possessiveReplace = "Papers",

                regular = "Devara",
                regularReplace = "Papers",

                active = true
            },

            // Spanish?
            new god_language_strings()
            {
                possessive = "Devara",
                possessiveReplace = "Papers",

                regular = "Devara",
                regularReplace = "Papers",
                active = true
            },

            // Spanish pt 2?
            new god_language_strings()
            {
                possessive = "Devara",
                possessiveReplace = "Papers",

                regular = "Devara",
                regularReplace = "Papers",
                active = true
            },

            // Asian langauge 1
            new god_language_strings()
            {
                active = false
            },

            // Asian langauge 2
            new god_language_strings()
            {
                active = false
            },

            // Asian langauge 3
            new god_language_strings()
            {
                active = false
            },

            // Russian
            new god_language_strings()
            {
                // Oh god I hope it's right.
                possessive = "Девары",
                possessiveReplace = "Пейпрзa",

                regular = "Девара",
                regularReplace = "Пейпрз",
                active = true
            },

            // Polish
            new god_language_strings()
            {
                // Oh god I hope it's right x2
                possessive = "Devary",
                possessiveReplace = "Papersa",

                regular = "Devara",
                regularReplace = "Papers",
                active = true
            },

            // Asian langauge 4
            new god_language_strings()
            {
                active = false
            },

            // English Debug
            new god_language_strings()
            {
                possessive = "Devara's",
                possessiveReplace = "Papers's",

                regular = "Devara",
                regularReplace = "Papers",
                active = true
            }
        };
        
        public override void Init()
        {
            ModHooks.Instance.DialogStringsLoaded += papersNice;
        }

        private static void papersNice(List<LocPair> dialog, NPCDialog[] npcDiag)
        {
            foreach (LocPair p in dialog)
            {
                //if (p.orig != "CREED_CLERIC" && !p.orig.Contains("DEVARA")) return;

                for (int i = 0; i < p.locStr.Length; i++)
                {
                    if (LANGUAGE_STRINGS.Length >= i) continue;
                    if (!LANGUAGE_STRINGS[i].active) continue;

                    if (p.locStr[i].Contains(LANGUAGE_STRINGS[i].possessive))
                    {
                        p.locStr[i] = p.locStr[i].Replace(LANGUAGE_STRINGS[i].possessive, LANGUAGE_STRINGS[i].possessiveReplace);
                    } else if (p.locStr[i].Contains(LANGUAGE_STRINGS[i].regular))
                    {
                        p.locStr[i] = p.locStr[i].Replace(LANGUAGE_STRINGS[i].regular, LANGUAGE_STRINGS[i].regularReplace);
                    }
                    
                }
                //Logger.Log("Found string: [" + p.orig + "] = " + p.locStr[0]);
            }
            //Logger.Log("NPC Dialogue length is " + npcDiag.Length);

            foreach (NPCDialog d in npcDiag)
            {
                if (d == null)
                    continue;
                
                //Logger.Log( "[" + d.name + "] length is " + d.nodeList.Length);
                foreach (DialogNode dn in d.nodeList)
                {
                    if (dn == null)
                        continue;

                    //Logger.Log("[" + d.name + "] [" + dn.name + "] length is " + dn.text.Length);
                    
                    for (int i = (dn.text.Length - 1); i >= 0; i--)
                    {
                        if (dn.text[i] == null)
                            continue;

                        if (dn.text[i].text == null)
                        {
                            continue;
                        }
                        
                        if (i > 12)
                        {
                            Logger.Log("Invalid language number " + i);
                            continue;
                        }
                        if (!LANGUAGE_STRINGS[i].active) continue;

                        for (int j = (dn.text[i].text.Length - 1); j >= 0; j--)
                        {
                            if (dn.text[i].text[j] == null)
                                continue;
                            

                            if (dn.text[i].text[j].Contains(LANGUAGE_STRINGS[i].possessive))
                            {
                                Logger.Log("Found devara's string [" + d.name + "][" + dn.name + "] in language " + i + " with text: " + dn.text[i].text[j]);
                                
                                dn.text[i].text[j] = dn.text[i].text[j].Replace(LANGUAGE_STRINGS[i].possessive,
                                    LANGUAGE_STRINGS[i].possessiveReplace);
                            }
                            if (dn.text[i].text[j].Contains(LANGUAGE_STRINGS[i].regular))
                            {
                                Logger.Log("Found devara string [" + d.name + "][" + dn.name + "] in language " + i + " with text: " + dn.text[i].text[j]);
                                
                                dn.text[i].text[j] = dn.text[i].text[j].
                                    Replace(LANGUAGE_STRINGS[i].regular, LANGUAGE_STRINGS[i].regularReplace);
                            }
                        }
                    }
                }
            }
            
            Modding.Logger.Log("That's all, folks");
        }
    }
}